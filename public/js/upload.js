$(document).ready(function() {
  var dropZone = $("#dropZone");
  var jqxhrList = {};
  dropZone[0].ondragover = function() {
    return false;
  };

  dropZone[0].ondrop = function(event) {
    var SIGN = uuid.v4();
    event.preventDefault();
    var files = event.dataTransfer.files;
    $("#fileList").empty();

    $.get("/html/progressBar.html", function (data) {
      for (var ind = 0; ind < files.length; ind++) {
        var el = "<li class='list-group-item' id=" + ind + ">" + files[ind].name + "</li>";
        var buttonUpload = '<button type="button" class="btn btn-default btn-sm btnUpload"><span class="glyphicon glyphicon-upload"></span></button>';
        var buttonCancel = '<button type="button" class="btn btn-default btn-sm btnCancel"><span class="glyphicon glyphicon-remove"></span></button>';
        $("#fileList").append(el);
        $("#" + ind).append(data).append(buttonUpload).append(buttonCancel);

        $("li#"+ ind + " > button.btnUpload").click(function(event) {
          var id = event.target.id;
          if (!id) id = event.target.offsetParent.id;
          resetClassProgressBar(id);
          uploadOne(files[id], id, SIGN);
        });

        $("li#"+ ind + " > button.btnCancel").click(function(event) {
          var id = event.target.parentElement.offsetParent.id;
          if (!id) id = event.target.parentElement.id;
          cancelUploadOne(id);
        })
      }
    });

    $("#uploadAll").click(function () {
      uploadAll(files, SIGN);
    });

  };

  function cancelUploadOne(ind) {
    var el = $("ul li:eq(" + ind + ") > div > div");
    if (jqxhrList[ind] && jqxhrList[ind].readyState != 4) {
      console.log("abort");
      jqxhrList[ind].abort();
      progressBarIndicate(el, 0);
    }
  }

  function onDone(data) {
    console.log(data);
    var el = $("#fileList li:eq(" + data.index + ")");
    el.append("<p>" + data.ref + "</p>")
  }

  function uploadAll(files, SIGN) {
    if(!files) return false;

    for (var i = 0; i < files.length; i++) {
      uploadOne(files[i], i, SIGN)
    }
  }

  function uploadOne(file, ind, SIGN) {
    var formData = new FormData();
    formData.append("file", file);
    formData.append("index", ind);
    formData.append("sign", SIGN);
    formData.append("fileName", file.name);

    var jqxhr = $.ajax({
      type: "POST",
      url: "upload",
      processData: false,
      contentType: false,
      async: true,
      data: formData,
      context: ind,
      success: function(data, text, xhr) {
        onComplete(xhr, ind);
      },
      error: function() {
        onError(ind);
      },
      xhr: function() {
        var xhr = $.ajaxSettings.xhr();
        var fileNumber = ind;
        xhr.upload.addEventListener("progress", function(e) {

          var el = $("ul li:eq(" + fileNumber + ") > div > div");
          if(e.lengthComputable) {
            var percent = Math.ceil(e.loaded/e.total * 100);
            progressBarIndicate(el, percent);
          }
        });
        return xhr
      }
    }).done(onDone);
    jqxhrList[ind] = jqxhr;
  }

  function progressBarIndicate(el,percent) {
    el.attr("style", "width:" + percent + "%");
    el.find("span").text(percent + "%");
  }

  function onComplete(xhr, ind) {
    console.log("COMPLETE");
    if (xhr.status != 200) {
      onDone(ind)
    }
  }

  function onError(ind) {
    var el = $("ul li:eq(" + ind + ") > div > div");
    el.addClass("progress-bar-danger");
    el.text("ERROR");
  }

  function resetClassProgressBar(ind) {
    var el = $("ul li:eq(" + ind + ") > div > div");
    el.removeClass("progress-bar-danger");
  }
})

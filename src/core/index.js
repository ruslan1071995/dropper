"use strict";
const config_1 = require("../../config");
const db_1 = require("./db");
const core = {
    conf: config_1.default,
    db: db_1.default
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = core;

"use strict";
const Sequelize = require("sequelize");
const path = require("path");
const config_1 = require("../../../config");
const dbConf = config_1.default.get("db");
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new Sequelize(dbConf.database, dbConf.username, dbConf.password, {
    storage: path.join(process.env.PATH, dbConf.storage),
    dialect: "sqlite"
});

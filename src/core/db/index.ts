import * as Sequelize from "sequelize";
import * as path from "path"
import conf from "../../../config"

const dbConf = conf.get("db");

export default new Sequelize(dbConf.database, dbConf.username, dbConf.password, {
  storage: path.join(process.env.PATH, dbConf.storage),
  dialect: "sqlite"
})

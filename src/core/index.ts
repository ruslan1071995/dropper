import conf from "../../config";
import db from "./db"

const core = {
  conf,
  db
}

export default core

import * as fs from "fs";
import * as path from "path";


class FolderService {
  static create(target: string): any {
    return FolderService.folderIsAlreadyExists(target)
    .then((ans: any) => {
      if (!ans.result) fs.mkdir(ans.PATH, function(err) {
        if (err) throw err
      })
      else throw new Error("Can't create directory")
      return ans.PATH
    }).catch((err: Error) => {
      console.log(err.message)
    })
  }

  static delete(target: string) {
    return FolderService.folderIsAlreadyExists(target)
    .then((ans: any) => {
      if (ans.result) fs.rmdir(ans.PATH)
      else throw new Error("Can't delete directory")
      return ans.PATH
    }).catch((err: Error) => {
      console.log(err.message)
    })
  }

  static folderIsAlreadyExists(target: string) {
    return new Promise((res, rej) => {

      const PATH: string = FolderService.preparePath(target);

      fs.access(PATH, fs.F_OK, (err) => {
        if (err) {
          res({PATH, result: false})
        } else res({PATH, result: true})
      })
    })
  }

  static listAllFiles(path: string) {
    return fs.readdirSync(path);
  }

  private static preparePath(target: string): string {
    const PREPATH = path.join(process.env.PATH, "temp");
    const PATH: string = path.join(PREPATH, target)
    return PATH;
  }
}

export default FolderService
"use strict";
const fs = require("fs");
const path = require("path");
class FolderService {
    static create(target) {
        return FolderService.folderIsAlreadyExists(target)
            .then((ans) => {
            if (!ans.result)
                fs.mkdir(ans.PATH, function (err) {
                    if (err)
                        throw err;
                });
            else
                throw new Error("Can't create directory");
            return ans.PATH;
        }).catch((err) => {
            console.log(err.message);
        });
    }
    static delete(target) {
        return FolderService.folderIsAlreadyExists(target)
            .then((ans) => {
            if (ans.result)
                fs.rmdir(ans.PATH);
            else
                throw new Error("Can't delete directory");
            return ans.PATH;
        }).catch((err) => {
            console.log(err.message);
        });
    }
    static folderIsAlreadyExists(target) {
        return new Promise((res, rej) => {
            const PATH = FolderService.preparePath(target);
            fs.access(PATH, fs.F_OK, (err) => {
                if (err) {
                    res({ PATH: PATH, result: false });
                }
                else
                    res({ PATH: PATH, result: true });
            });
        });
    }
    static listAllFiles(path) {
        return fs.readdirSync(path);
    }
    static preparePath(target) {
        const PREPATH = path.join(process.env.PATH, "temp");
        const PATH = path.join(PREPATH, target);
        return PATH;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = FolderService;

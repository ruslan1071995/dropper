"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (sequelize, DATATYPE) => {
    const Ref = sequelize.define('refs', {
        ref: {
            type: DATATYPE.STRING,
            primaryKey: true
        },
        dir: {
            type: DATATYPE.STRING,
            allowNull: false,
            unique: true
        },
        sign: {
            type: DATATYPE.STRING,
            allowNull: false,
            unique: false
        }
    }, {
        tableName: 'refs',
        timestamps: false,
    });
    return Ref;
};

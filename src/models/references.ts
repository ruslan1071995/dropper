export interface References {
  ref: string,
  dir: string,
  sign: string
}


export default (sequelize: any, DATATYPE: any) => {
  const Ref = sequelize.define('refs', {
    ref: {
      type: DATATYPE.STRING,
      primaryKey: true
    },
    dir: {
      type: DATATYPE.STRING,
      allowNull: false,
      unique: true
    },
    sign: {
      type: DATATYPE.STRING,
      allowNull: false,
      unique: false
    }
  }, {
    tableName: 'refs',
    timestamps: false,
  });

  return Ref
}

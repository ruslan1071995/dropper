import refs from "./references"
import * as Sequelize from "sequelize";
import sequelize from "../core/db"

export interface database {
  Sequelize: any,
  sequelize: Sequelize.Sequelize,
  References: any // TODO Sequelize.Model<TInstance, IAttributes>
}

let db = <database>{}



db["References"] = refs(sequelize, Sequelize);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;

"use strict";
const express = require('express');
const path = require("path");
const routes_1 = require("./routes");
const app = express();
const port = process.env.PORT || 80;
app.use(express.static(path.join(process.env.PATH, "public")));
app.use(routes_1.default);
app.listen(port, () => {
    console.log("Server running");
});

"use strict";
const express_1 = require("express");
const UploadCtrl_1 = require("../controllers/UploadCtrl");
const Download_1 = require("../controllers/Download");
const fs = require("fs");
const path = require("path");
let router = express_1.Router();
router.get("/files/:id", Download_1.default.downloadFile);
router.get("/", (req, res, next) => {
    console.log("get/");
    fs.createReadStream(path.join(process.env.PATH + "/temp/index.html")).pipe(res);
});
router.post("/upload", UploadCtrl_1.default.upload);
router.use((req, res, next) => {
    res.send("Page not found");
});
router.use((err, req, res, next) => {
    console.log(err.stack);
    res.status(500).send("Error");
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;

import {Router} from "express";
import UploadCtrl from "../controllers/UploadCtrl";
import DownloadCtrl from "../controllers/Download";
import * as fs from "fs";
import * as path from "path";

let router = Router();

router.get("/files/:id", DownloadCtrl.downloadFile);

router.get("/", (req, res, next) => {
  console.log("get/");
  fs.createReadStream(path.join(process.env.PATH + "/temp/index.html")).pipe(res);
});

router.post("/upload", UploadCtrl.upload);

router.use((req, res, next) => {
  res.send("Page not found")
});

router.use((err: Error, req, res, next) => {
  console.log(err.stack);
  res.status(500).send("Error")
});

export default router

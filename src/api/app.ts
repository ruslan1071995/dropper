import * as express from 'express';
import * as path from "path";
import core from "../core";
import fileRoute from "./routes";
import FolderService from "../core/services/FolderService"
const app = express();
const port:number = process.env.PORT || 80;


app.use(express.static(path.join(process.env.PATH, "public")));
app.use(fileRoute);

app.listen(port, () => {
  console.log("Server running")
});


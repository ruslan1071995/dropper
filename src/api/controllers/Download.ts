import db from "../../models/";
import config from "../../../config";
import FolderService from "../../core/services/FolderService";
import {Request, Response} from "express"

class DownloadCtrl {
  static downloadFile(req:Request, res:Response, next: any): void {
    db.References.findOne({
      where: {
        ref: req.params.id
      }
    }).then((value: any) => {
      if (!value) throw new Error("404");
      const DIR = value.dataValues.dir;
      return FolderService.folderIsAlreadyExists(DIR)
    }).then((ans: any) => {
      if (!ans) throw new Error("400");
      if (!ans.result) throw new Error("403")
      let listFiles = FolderService.listAllFiles(ans.PATH);
      res.download(`${ans.PATH}/${listFiles[0]}`)
    }).catch((err: Error) => {
      next();
    })
  }
}

export default DownloadCtrl

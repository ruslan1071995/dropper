"use strict";
const fs = require("fs");
const path = require("path");
const formidable = require("formidable");
const uuid = require("node-uuid");
const rimraf = require("rimraf");
const FolderService_1 = require("../../core/services/FolderService");
const _1 = require("../../models/");
class UploadCtrl {
    static upload(req, res, next) {
        console.log("post/upload");
        var form = new formidable.IncomingForm();
        const FOLDER = uuid.v4();
        const REF = uuid.v4();
        let fileExist = { is: false };
        let field;
        form.parse(req, (err, fields, files) => {
            field = fields;
        });
        UploadCtrl.addFileBeginListner(form, FOLDER);
        UploadCtrl.addFileListener(form, fileExist);
        UploadCtrl.addErrorListener(form, next);
        form.on("end", () => {
            if (!fileExist.is)
                return next(501);
            _1.default.sequelize.sync().then(() => {
                return _1.default.References.create({
                    dir: FOLDER,
                    ref: REF,
                    sign: field.sign
                });
            }).then(() => {
                res.json({
                    ref: `<a href="/files/${REF}"><button type="button" class="btn btn-info">${field.fileName}</button></a>`,
                    index: field.index
                });
            });
        });
    }
    static addFileBeginListner(form, FOLDER) {
        form.on("fileBegin", () => {
            FolderService_1.default.create(FOLDER)
                .then((result) => {
                form.uploadDir = result;
                form.keepExtensions = true;
                form.maxFields = 0;
            });
        });
    }
    static addFileListener(form, fileExist) {
        form.on("file", (name, file) => {
            fileExist.is = true;
            fs.rename(file.path, path.join(form.uploadDir, file.name));
        });
    }
    static addErrorListener(form, next) {
        form.on("aborted", () => {
            console.log("aborted");
            rimraf(form.uploadDir, (err) => {
                if (err)
                    return next(500);
            });
        });
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UploadCtrl;

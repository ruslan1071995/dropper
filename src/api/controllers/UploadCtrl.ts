import * as fs from "fs"
import * as path from "path"
import * as formidable from "formidable"
import * as uuid from "node-uuid"
import * as rimraf from "rimraf"
import FolderService from "../../core/services/FolderService";
import db from "../../models/";
import conf from "../../../config";

interface propFields extends formidable.Fields{
  ref: string,
  index: string,
  sign: string,
  fileName: string
}

class UploadCtrl {

  static upload(req: any, res: any, next: Function): any { // res: Express.Response warn about res.json()
    console.log("post/upload");
    var form = new formidable.IncomingForm();
    const FOLDER: string = uuid.v4();
    const REF: string = uuid.v4();
    let fileExist: any = {is: false};
    let field: propFields;

    form.parse(req, (err: Error, fields:propFields, files:formidable.Files) => {
      field = fields;
    });

    UploadCtrl.addFileBeginListner(form, FOLDER);
    UploadCtrl.addFileListener(form, fileExist);
    UploadCtrl.addErrorListener(form, next);

    form.on("end", () => { // TODO Вынести в отдельную функцию
      if(!fileExist.is) return next(501);
      db.sequelize.sync().then(() => {
        return db.References.create({
          dir: FOLDER,
          ref: REF,
          sign: field.sign
        })
      }).then(() => {
        res.json({
          ref: `<a href="/files/${REF}"><button type="button" class="btn btn-info">${field.fileName}</button></a>`,
          index: field.index
        })
      })
    })
  }

  private static addFileBeginListner(form: formidable.IncomingForm, FOLDER: string){
    form.on("fileBegin", () => {
      FolderService.create(FOLDER)
      .then((result: any) => {
        form.uploadDir = result;
        form.keepExtensions = true;
        form.maxFields = 0;
      })
    })
  }

  private static addFileListener(form: formidable.IncomingForm, fileExist: any) {
    form.on("file", (name: string, file:formidable.File) => {
      fileExist.is = true;
      fs.rename(file.path, path.join(form.uploadDir, file.name))
    })
  }

  private static addErrorListener(form: formidable.IncomingForm, next: Function) {
    form.on("aborted", () => {
      console.log("aborted");
      rimraf(form.uploadDir, (err) => {
        if (err) return next(500)
      })
    })
  }
}

export default UploadCtrl

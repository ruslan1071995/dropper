"use strict";
const _1 = require("../../models/");
const FolderService_1 = require("../../core/services/FolderService");
class DownloadCtrl {
    static downloadFile(req, res, next) {
        _1.default.References.findOne({
            where: {
                ref: req.params.id
            }
        }).then((value) => {
            if (!value)
                throw new Error("404");
            const DIR = value.dataValues.dir;
            return FolderService_1.default.folderIsAlreadyExists(DIR);
        }).then((ans) => {
            if (!ans)
                throw new Error("400");
            if (!ans.result)
                throw new Error("403");
            let listFiles = FolderService_1.default.listAllFiles(ans.PATH);
            res.download(`${ans.PATH}/${listFiles[0]}`);
        }).catch((err) => {
            next();
        });
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = DownloadCtrl;

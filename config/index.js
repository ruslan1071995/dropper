"use strict";
const nconf = require("nconf");
const path = require("path");
const CONFIG_FILE = path.join(__dirname, "config.json");
nconf.file({ file: CONFIG_FILE });
console.log("config");
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = nconf;

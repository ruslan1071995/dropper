import * as nconf from "nconf"
import * as path from "path"

const CONFIG_FILE = path.join(__dirname, "config.json");

nconf.file({file: CONFIG_FILE})

console.log("config")

export default nconf
